class Polygon {
    constructor(array) {
        this.totalPerimeter = array.reduce((a, b) => a + b, 0);
    }
    perimeter() {
        return this.totalPerimeter;
    }
}