function getCount(objects) {
    let numberCoincidences = 0;
    for (let i = 0; i < objects.length;i++){
        if (objects[i].x == objects[i].y) {
            numberCoincidences++;
        }
    }
    return numberCoincidences;
}