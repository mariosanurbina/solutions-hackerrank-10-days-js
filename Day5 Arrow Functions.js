function modifyArray(nums) {
    let newArray = nums.map((x) => (x % 2 == 0) ? x * 2 : x * 3);
    return newArray;
}
